/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Rubinho
 */
public class PontoXZ extends Ponto2D {
    private double x,z;
    
    public PontoXZ() {
    }

    public PontoXZ(double x, double z) {
        super(x, 0, z);
        this.x=x; this.z=z;
    }
         /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    @Override
    public String getNome() {
        System.out.println(getClass());
        return getClass().getSimpleName()+String.format("(%1$f,%2$f)",x,z);
    }

    
}
