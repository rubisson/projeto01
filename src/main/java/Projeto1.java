
import utfpr.ct.dainf.if62c.projeto.PontoXY;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;
import utfpr.ct.dainf.if62c.projeto.PontoYZ;



/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        PontoXY obj1 = new PontoXY(0,2);
        PontoYZ obj2 = new PontoYZ(0,1);
        PontoXZ obj3 = new PontoXZ(0,1);

        String str="aaa";
        
        System.out.println("nome não qualificado da classe >>> " + obj1.getNome());
        System.out.println("nome não qualificado da classe >>> " + obj2.getNome());
        System.out.println("nome não qualificado da classe >>> " + obj3.getNome());
    
        if (obj1.equals(obj2)) {
         System.out.println("distância entre dois pontos " + obj2.dist(obj1));
        }
    }
    
}
